<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Assets/style.css">
    
    <title>CredPlus</title>
</head>
<body>
<div class="login-page">
  <div class="form">
    <div class="icon">
          <img src="https://ibankdecla.com.br/img/logo_0003.png" alt="">
    </div>
    </form>
    <form class="login-form">
      <input type="text" placeholder="Username:"/>
      <input type="password" placeholder="Password:"/>
      <div id="inputPreview">
          <input name="cssCheckbox" id="demo_opt_1" type="radio" class="css-checkbox" checked="">
          <label for="demo_opt_1">HTML5</label>
          <input name="cssCheckbox" id="demo_opt_2" type="radio" class="css-checkbox" >
          <label for="demo_opt_2">RemoteApp</label>
      </div>
      <button>log on</button>
    </form>
  </div>
</div>
</body>
<script>
  $('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
  });
</script>
</html>
